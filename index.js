function scrollTo(elementId) {
	$('html, body').animate({
        scrollTop: $(elementId).offset().top - 90
    }, 1000);
}