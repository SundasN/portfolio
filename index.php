<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
  xml:lang="fr" lang="fr">
	<head>
		<style type="text/css">
        </style>
        <script type="text/javascript">
            function myFunction() {
			    var x = document.getElementById("myTopnav");
			    if (x.className === "topnav") {
			        x.className += " responsive";
			    } 
			    else {
			        x.className = "topnav";
			    }
			}
        </script>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="views/css/index.css">
		<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="views/index.js"></script>
	    <script> 
		    $(function(){
		      $("#includedContent").load("views/welcome.inc.php"); 
		    });
	    </script>

		<title>
			my portfolio
		</title>	
	</head>
	<body>
		<header id="header">
			<div id="name">
				<h1 class="name">SUNDAS NIAZ</h1>
			</div>
			<img id="logo" src="views/images/logo.png">
			<ul class="topnav" id="myTopnav">
				<li class="item">
			  		<button onclick="scrollTo('#about')">About</button>
				</li>
				<li class="item">
			  		<button onclick="scrollTo('#work')">Work</button>
			  	</li>
				<li class="item">
					<button onclick="scrollTo('#contact')">Contact</button>
				</li>
				<li class="icon">
			    	<a href="javascript:void(0);" onclick="myFunction()">&#9776;</a>
				</li>
			</ul>
			<div id="clear"></div>
		</header>

		<div id="container">
			<div id ="content">
				<div id="includedContent">
				</div>
				<div id="content-data">
					<section id="about">	
						<?php include('views/about.inc.php'); ?>
					</section>
					<img id="separator-image" src="views/images/separator.png">

					<section id="work">
						<?php include('views/work.inc.php'); ?>
					</section>
					<img id="separator-image" src="views/images/separator.png">

					<section id="contact">
						<?php include('views/contact.inc.php'); ?>
					</section>
				</div>	
			</div> 
		</div>

		<script type="text/javascript">
			$(document).ready(function(){
    			$("#nav-mobile").html($("#menu").html());
    			$("#nav-trigger span").click(function(){
       				if ($("nav#nav-mobile button").hasClass("expanded")) {
            			$("nav#nav-mobile button.expanded").removeClass("expanded").slideUp(250);
           				$(this).removeClass("open");
       				} 
       				else {
			            $("nav#nav-mobile button").addClass("expanded").slideDown(450);
			            $(this).addClass("open");
			        }
			    });
			});
		</script>
	</body>
</html>