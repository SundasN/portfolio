<!DOCTYPE html>
<html>
	<head>
		<title>
			my portfolio
		</title>	
		<link rel="stylesheet" type="text/css" href="views/engine1/style.css" />
		<link rel="stylesheet" type="text/css" href="views/css/work.css" />
		<script type="text/javascript" src="views/engine1/jquery.js"></script>
		<link href='https://fonts.googleapis.com/css?family=Simonetta:900italic' rel='stylesheet' type='text/css'>
	</head>
	<body>
		<main id="main-work">
			<h2 id="my-work"> My work </h2>
		</main>
		<div id="wowslider-container1">
			<div class="ws_images">
				<ul>
					<li><img src="views/data1/images/image1.png" alt="image1" title="image1" id="wows1_0"/></li>
					<li><img src="views/data1/images/image2.png" alt="image2" title="image2" id="wows1_1"/></li>
					<li><img src="views/data1/images/image3.png" alt="image3" title="image3" id="wows1_2"/></li>
					<li><img src="views/data1/images/image4.png" alt="image4" title="image4" id="wows1_3"/></li>
					<li><img src="views/data1/images/image5.png" alt="image5" title="image5" id="wows1_4"/></li>
					<li><img src="views/data1/images/image6.png" alt="image6" title="image6" id="wows1_5"/></li>
					<li><img src="views/data1/images/image7.png" alt="image7" title="image7" id="wows1_6"/></li>
					<li><img src="views/data1/images/image8.png" alt="image8" title="image8" id="wows1_7"/></li>
					<li><img src="views/data1/images/image9.png" alt="image9" title="image9" id="wows1_8"/></li>
					<li><img src="views/data1/images/image10.png" alt="image10" title="image10" id="wows1_9"/></li>
					<li><img src="views/data1/images/image11.png" alt="image11" title="image11" id="wows1_10"/></li>
					<li><img src="views/data1/images/image12.png" alt="image12" title="image12" id="wows1_11"/></li>
					<li><img src="views/data1/images/image13.png" alt="image13" title="image13" id="wows1_12"/></li>
					<li><img src="views/data1/images/image14.png" alt="image14" title="image14" id="wows1_13"/></li>
					<li><img src="views/data1/images/image15.png" alt="image15" title="image15" id="wows1_14"/></li>
					<li><img src="views/data1/images/image16.png" alt="image16" title="image16" id="wows1_15"/></li>
					<li><img src="views/data1/images/image17.png" alt="image17" title="image17" id="wows1_16"/></li>
					<li><img src="views/data1/images/image18.png" alt="image18" title="image18" id="wows1_17"/></li>
					<li><img src="views/data1/images/image19.png" alt="image19" title="image19" id="wows1_18"/></li>
					<li><img src="views/data1/images/image20.png" alt="image20" title="image20" id="wows1_19"/></li>
					<li><img src="views/data1/images/image21.png" alt="image21" title="image21" id="wows1_20"/></li>
					<li><img src="views/data1/images/image22.png" alt="image22" title="image22" id="wows1_21"/></li>
					<li><img src="views/data1/images/image23.png" alt="image23" title="image23" id="wows1_22"/></li>
					<li><img src="views/data1/images/image24.png" alt="http://wowslider.net/" title="image24" id="wows1_23"/></li>
					<li><img src="views/data1/images/image25.png" alt="image25" title="image25" id="wows1_24"/></li>
				</ul>
			</div>
			<div class="ws_bullets">
				<div>
					<a href="#" title="image1"><span><img src="views/data1/tooltips/image1.png" alt="image1"/>1</span></a>
					<a href="#" title="image2"><span><img src="views/data1/tooltips/image2.png" alt="image2"/>2</span></a>
					<a href="#" title="image3"><span><img src="views/data1/tooltips/image3.png" alt="image3"/>3</span></a>
					<a href="#" title="image4"><span><img src="views/data1/tooltips/image4.png" alt="image4"/>4</span></a>
					<a href="#" title="image5"><span><img src="views/data1/tooltips/image5.png" alt="image5"/>5</span></a>
					<a href="#" title="image6"><span><img src="views/data1/tooltips/image6.png" alt="image6"/>6</span></a>
					<a href="#" title="image7"><span><img src="views/data1/tooltips/image7.png" alt="image7"/>7</span></a>
					<a href="#" title="image8"><span><img src="views/data1/tooltips/image8.png" alt="image8"/>8</span></a>
					<a href="#" title="image9"><span><img src="views/data1/tooltips/image9.png" alt="image9"/>9</span></a>
					<a href="#" title="image10"><span><img src="views/data1/tooltips/image10.png" alt="image10"/>10</span></a>
					<a href="#" title="image11"><span><img src="views/data1/tooltips/image11.png" alt="image11"/>11</span></a>
					<a href="#" title="image12"><span><img src="views/data1/tooltips/image12.png" alt="image12"/>12</span></a>
					<a href="#" title="image13"><span><img src="views/data1/tooltips/image13.png" alt="image13"/>13</span></a>
					<a href="#" title="image14"><span><img src="views/data1/tooltips/image14.png" alt="image14"/>14</span></a>
					<a href="#" title="image15"><span><img src="views/data1/tooltips/image15.png" alt="image15"/>15</span></a>
					<a href="#" title="image16"><span><img src="views/data1/tooltips/image16.png" alt="image16"/>16</span></a>
					<a href="#" title="image17"><span><img src="views/data1/tooltips/image17.png" alt="image17"/>17</span></a>
					<a href="#" title="image18"><span><img src="views/data1/tooltips/image18.png" alt="image18"/>18</span></a>
					<a href="#" title="image19"><span><img src="views/data1/tooltips/image19.png" alt="image19"/>19</span></a>
					<a href="#" title="image20"><span><img src="views/data1/tooltips/image20.png" alt="image20"/>20</span></a>
					<a href="#" title="image21"><span><img src="views/data1/tooltips/image21.png" alt="image21"/>21</span></a>
					<a href="#" title="image22"><span><img src="views/data1/tooltips/image22.png" alt="image22"/>22</span></a>
					<a href="#" title="image23"><span><img src="views/data1/tooltips/image23.png" alt="image23"/>23</span></a>
					<a href="#" title="image24"><span><img src="views/data1/tooltips/image24.png" alt="image24"/>24</span></a>
					<a href="#" title="image25"><span><img src="views/data1/tooltips/image25.png" alt="image25"/>25</span></a>
				</div>
			</div>
			<div class="ws_script" style="position:absolute;left:-99%">
				<a href="http://wowslider.net">wowslider.net</a> by WOWSlider.com v8.7
			</div>
			<div class="ws_shadow">
			</div>
		</div>	
		<script type="text/javascript" src="views/engine1/wowslider.js"></script>
		<script type="text/javascript" src="views/engine1/script.js"></script>
	</body>
</html>