<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="views/css/about.css">
		<link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Simonetta:900italic' rel='stylesheet' type='text/css'>
		<title>
			my portfolio
		</title>	
	</head>

	<body>
		<main id="main-about">
		<h2 id="about-me">About Me </h2>
		<p> I'm Sundas Niaz. I live in Le Blanc-Mesnil, France. I'm a web designer. 
			My goal is to create websites with my coding skills and to make them look attractive and appealing 
			I will be using my artistic skills.
		</p>
		<p> In my spare time I like to read, listen to music and watch movies. 
			I also love photography, two things I love most to capture with my camera are trees and my rabbit. 
		</p>
		<p> I graduated from Mozart High School in 2011 with biology as my speciality. 
			Then I went to premedical school but soon I realized that it wasn't my cup of tea. 
			After a break and alot of research I chose to persue my career as a webdesigner. 
			So I took preparatory class in arts in Koronin (2012-2013) to refine my art skills and then 
			I joined 3WA to learn programming.
		</p>
		<p> As I mentioned earlier I went to Koronin, where I learned painting, sketching and sculpting. 
			And I also learned how to use Illustrator.  To try and learn other tools I taught myself how to use
			Photoshop and Gimp. 
		</p>

		<h3>
			Skills: 
		</h3>	
		<ul class="bargraph">
		</ul>

		<ul class="bargraph" id="bargraph">
			<li class="first-bar">XHTML / CSS</li>
			<li class="second-bar">WordPress</li>
		    <li class="third-bar">Gimp</li>
		    <li class="fourth-bar">Photoshop</li>
		    <li class="fifth-bar">PHP</li>
			<li class="sixth-bar">MySQL</li>
		    <li class="seventh-bar">JS</li>
		    <li class="eighth-bar">Jquery</li>
		    <li class="ninth-bar">Illustrator</li>
			<li class="tenth-bar">Bootstrap</li>
		</ul>
		</main>

	</body>
</html>