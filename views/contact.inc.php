<?php
	include("config.php");
?>

<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="views/css/contact.css">
		<link href='https://fonts.googleapis.com/css?family=Simonetta:900italic' rel='stylesheet' type='text/css'>
	</head>
	
	<body>
		<main id="main-contact">
		<h2 id="contact"> Feel free to contact me. </h2>
		<div class="form-wrapper">
			<form name="user" action="views/user_process.php" method="post" id="contact-form">
				<label>Name <span>*</span></label>
				<input type="text" name="name" value="Name" id="form-name" onfocus="this.value = this.value=='Name'?'':this.value;" onblur="this.value = this.value==''?'Name':this.value;"><br>
			 
				<label>City <span>*</span></label>
				<input type="text" name="city" value="City" id="city" onfocus="this.value = this.value=='City'?'':this.value;" onblur="this.value = this.value==''?'City':this.value;"><br>
			 	
			 	<label>Email <span>*</span></label>
				<input type="text" name="email" value="Em@il" id="email" onfocus="this.value = this.value=='Em@il'?'':this.value;" onblur="this.value = this.value==''?'Em@il':this.value;"><br>

				<label>Message</label>
				<textarea name="message" cols="28" rows="5" placeholder="Message" id="message" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Message'" /></textarea>
				
				<div class="box">
					<input type="submit" name="submit" value="Submit" id="submit_form" onClick="window.location.href='http://127.0.0.1:8080/portfolio/index.php#popup1'">
				</div> 
			</form>
		</div>

		<div id="popup-container"></div>
		<script type="text/javascript">
			$(document).ready(function () {
			    if(window.location.href.indexOf("success") != -1) {
			    	$( "#popup-container" ).html( "<div class=\"overlay\">"+
			    		"<div class=\"popup\" >"+
			    		"<a class=\"close\" href=\"index.php\"><img src=\"views/images/exit.png\" id=\"exitImage\"></a>"+
			    		"<div class=\"content\">"+
	    		       		"SUCCESS <br> <span> Your message has been submitted.</span>"+
	    				"</div>"+
	    				"</div>"+
	    				"</div>" );
			    }
			    else{
			    	$( "#popup-container" ).html("");
			    }
			});
		</script>
		</main>
	</body>
</html>